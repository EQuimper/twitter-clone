import bodyParser from 'body-parser';
import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';

import typeDefs from '../../graphql/schema'
import resolvers from '../../graphql/resolvers/index';
import { decodeToken } from '../../services/auth';

import constant from '../config/constant';

export default app => {
  app.use(bodyParser.json());

  async function auth(req, res, next) {
    try {
      const token = req.headers.authorization;
      if (token != null) {
        const user = await decodeToken(token);
        req.user = user;
      } else {
        req.user = null;
      }
      next();
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers
  });

  app.use(auth);

  app.use('/graphiql', graphiqlExpress({
    endpointURL: constant.GRAPHQL_PATH
  }));

  app.use(constant.GRAPHQL_PATH, graphqlExpress(req => ({
    schema,
    context: {
      user: req.user
    }
  })));
}