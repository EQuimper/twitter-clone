import jwt from 'jsonwebtoken';

import User from '../mongodb/models/Users';
import constant from '../mongodb/config/constant';

export async function requireAuth(user) {
  if (!user || !user._id) {
    throw new Error('Unauthorized!');
  }
  const users = await User.findById(user._id);
  if (!users) {
    throw new Error(`UnAuthorized!`);
  }
  return users;
}


export function decodeToken(token) {
  try {
    const array = token.split(' ');
    if (array[0] === "Bearer") {
      return jwt.verify(array[1], constant.SECRET_KEY);
    }
    throw new Error('Invalid token');
  } catch (error) {
    console.error(error);
  }
}