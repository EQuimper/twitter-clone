import express from 'express';
import middleware from './mongodb/config/middleware';
import { createServer } from 'http';

import constant from './mongodb/config/constant';

//mongoDB configuration
import './mongodb/config/db';

//App is initialized
const app = express();

//middleware are made
middleware(app);

//graphql server is created
const graphQlServer = createServer(app)

//server is started
graphQlServer.listen(constant.PORT, err => {
    if (err) {
        console.error(err);
    } else {
        console.log(`App is succesfully running at ${constant.PORT}`);
    }
});