import Tweet from '../../mongodb/models/Tweets';
import { requireAuth } from '../../services/auth';

export default {
  getTweet: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      return Tweet.findById(_id)
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  getTweets: async (_, args, { user }) => {
    try {
      // await requireAuth(user);
      return Tweet.find({}).sort({ createdAt: -1 });
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  createTweet: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      return Tweet.create({ ...args, user: user._id });
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  getUserTweet: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      return Tweet.find({ user: user._id }).sort({ createdAt: -1 });
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  updateTweet: async (_, { _id, ...text }, { user }) => {
    try {
      await requireAuth(user);

      const tweet = await Tweet.findOne({ _id, user: user._id });

      if (!tweet) {
        throw new Error('Not Found');
      }

      Object.entries(text).forEach((key, value) => {
        tweet[key] = value;
      })

      return tweet.save();
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
  deleteTweet: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      const res = await Tweet.findOne({ _id, user: user._id });

      if (!res) {
        throw new Error(`Not Found!`);
      }
      await res.remove();
      return { message: "Success" }
    } catch (error) {
      console.error(error);
      return { message: "Fail to delete" }
    }
  }
}