import React, { Component } from "react";
import { StatusBar } from "react-native";
import { graphql } from "react-apollo";

import { GET_ALL_TWEETS } from "../../graphql/query/index";

import FeedCard from "../../components/FeedCard";

import { Root, List } from "./style";
import { colors } from "../../config/colors";

class App extends Component {
  render() {
    console.log(this.props.data);
    if (this.props.data.error) {
      return (
        <List>
          <FeedCard />
        </List>
      );
    }
    return (
      <Root>
        <StatusBar
          backgroundColor={colors.LIGHT_GRAY}
          barStyle="dark-content"
          showHideTransition="slide"
          networkActivityIndicatorVisible={false}
          animated={true}
        />
        <List>
          <FeedCard />
          <FeedCard />
          <FeedCard />
          <FeedCard />
          <FeedCard />
          <FeedCard />
          <FeedCard />
          <FeedCard />
        </List>
      </Root>
    );
  }
}

export default graphql(GET_ALL_TWEETS)(App);
