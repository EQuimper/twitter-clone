export const colors = {
  PRIMARY: '#55ACEE',
  SECONDARY: '#444B52',
  WHITE: '#FFFFFF',
  LIGHT_GRAY: '#CAD0D6',
}

export const avatar = "https://pbs.twimg.com/profile_images/947338427157975040/6B9ehRLY_bigger.jpg";