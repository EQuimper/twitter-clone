import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import ApolloClient, { createNetworkInterface } from "apollo-client";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";

import reducers from "../reducers/index";

const networkinterface = createNetworkInterface({
  uri: "http://localhost:3001/graphql"
});

export const client = new ApolloClient({
  networkinterface,
  connectToDevTools: true
});

const logger = createLogger({
  diff: true
});

const middleware = [client.middleware(), thunk, logger];

export const store = createStore(
  reducers(client),
  undefined,
  composeWithDevTools(applyMiddleware(...middleware))
);
