import style from 'styled-components/native';
import Touchable from '@appandflow/touchable';
// import Touchable from '../../../node_modules/@appandflow/touchable/src/Touchable';

export const Root = style.View`
height:40;
flexDirection:row;
`;

export const Button = style(Touchable).attrs({
  feedback: 'opacity',
})`
  flex: 1;
  flexDirection: row;
  alignItems: center;
  justifyContent: space-around;
  paddingHorizontal: 32px;
`;


export const ButtonText = style.Text`
fontSize:14;
fontWeight:500;
color:${props => props.theme.LIGHT_GRAY}
`;