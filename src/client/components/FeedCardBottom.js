import React from 'react';

import { Root, Button, ButtonText } from './FeedCardBottom.style';
import { SimpleLineIcons, Entypo } from '@expo/vector-icons';

import { colors } from '../config/colors';

const favouriteCount = 3;
const isFavourite = false;
const ICON_SIZE = 20;

function FeedCardBottom() {
  return (
    <Root>
      <Button>
        <SimpleLineIcons
          name="bubble"
          size={ICON_SIZE}
          color={colors.LIGHT_GRAY}
        />
        <ButtonText>
          {favouriteCount}
        </ButtonText>
      </Button>
      <Button>
        <Entypo name="retweet" color={colors.LIGHT_GRAY} size={ICON_SIZE} />
        <ButtonText>
          {favouriteCount}
        </ButtonText>
      </Button>
      <Button>
        <Entypo
          name="heart"
          color={isFavourite ? 'red' : colors.LIGHT_GRAY}
          size={ICON_SIZE}
        />
        <ButtonText>
          {favouriteCount}
        </ButtonText>
      </Button>
    </Root>
  )
}

export default FeedCardBottom;