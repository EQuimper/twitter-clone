import { router } from '../navigation';

export default (state, action) => {
  const Statenew = router.getStateForAction(action, state);
  return Statenew || state
}